Parse.initialize("JOiRQ4zeHDhhGbzr4oKw9QmKyrlpTcIH6k6uNzXY", "pnXpWoBz1Vx65MB38SNOVjcHnnQYG6L0jEpnVisJ");

// PARSE QUERY to kickoff app
var query = new Parse.Query('Task');

// Mount the components after query response
query.find().then(function(results) {

	/* SEE CONSOLE FOR DETAILED TRACE OF COMPONENTS */
	/* For Riot Guide/API > http://riotjs.com */
	riot.mount('sample', {
		tasks: results
	});
});

// For study purposes only
var x;