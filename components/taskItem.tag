<!-- TASKITEM Component -->
<taskItem>
	
	<!-- TEMPLATE -->
	<div class="task">
		<span class="featureList">
			<!-- Abstracted the feature to it's own component (See bottom of taskItem.tag) -->
			<!-- These get passed directly into component feature.feature, feature.i etc. -->
			<feature each={feature, i in data.features}></feature>	
		</span>
		
		
		<button onclick={addFeature}>Add Feature</button>
		<button onclick={removeFeature}>Remove Feature</button>
		<button onclick={resetFeatures}>Reset Features</button>
		<button onclick={setAscending}>Ascending</button>
		<button onclick={setDescending}>Descending</button>
		<span>---------- { data.createdAt }</span>
		<div class="editor">
			<p show={ !editMode }>{ data.task }</p>
			<input type="text" show={ editMode } value={ data.task } onkeypress={ onEnter }>
			<button onclick={ toggleEditor } show={ !editMode }>change task</button> <button onclick={ toggleEditor } show={ editMode }>cancel</button>	<button onclick={ save }>save</button>
		</div>
	</div>
	
	
	<!-- LOGIC -->
	<script>
		var that = this;

		console.log('<taskItem> created:', this)

		// Copy of reactive values
		this.data = this.model.toJSON();

		// Separate reactive value (from model data)
		this.editMode = false;

		// Actions
		// this.changeTask = function(event) {
		// 	this.data.task = 'changed task';
		// 	// that.update(); // If we directly access data on this component, they are reactive -> no need for manual updates
		// };

		this.save = function() {
			that.model.save(this.data);
		};

		// Try clicking on the task <p>
		this.toggleEditor = function(event) {
			that.editMode = !that.editMode;

			// Riot supplies observable events
			// on() one() -> 'update, updated, (un)mount related ...'
			this.one('updated', function() {
				$(this.root).find('input').focus();
			});
			
		};

		// Enter to set and save the 
		this.onEnter = function(event) {
			if (event.which === 13) {
				// No reason we can't use jquery here instead of native JS selectors

				// this.data.task is being watched by Riot
				this.data.task = $(this.root).find('input').val();
					// NOTE: have to test how this deals with Arrays and other
					// complex Objects

				this.model.save(this.data);
				this.toggleEditor();
			} else {
				return true;
			}
		};

		// Arrays seem to be observable even when nested. Cool.
		this.addFeature = function(event) {
			var featureText = prompt('add to array');
			this.data.features.push(featureText);
		};
		this.removeFeature = function(event) {
			var featureText = prompt('remove from array');
			var index = this.data.features.indexOf(featureText);
			this.data.features.splice(index, 1);
		};
		this.resetFeatures = function(event) {
			this.data.features = [];
		};
		this.setAscending = function() {
			// String sort
			this.data.features.sort();
			// Only for numbers
			// this.data.features.sort(function(a,b) {
			// 	return a - b;
			// });

			// Note: Natural sort is actually complex - not included to create less muck
		};
		this.setDescending = function() {
			// String sort
			this.data.features.reverse();
			// Only for numbers
			// this.data.features.sort(function(a,b) {
			// 	return b - a;
			// });
		};



		// Need to test nested objects and properties of.

		// Need to test removal of data / objects

		// For study purposes only
		x = this;

	</script>

	<!-- STYLE -->
	<style scoped>
		.task {
			border: 1px solid #333;
			margin-bottom: 10px;
			padding: 15px;
			border-radius: 6px;
		}
		.featureList {
			margin-right: 10px;
		}
		.editor {
			background-color: #DDD;
			padding: 1px 10px 10px;
			margin-top: 10px;
			border-radius: 5px;
		}
	</style>

</taskItem>



<!-- FEATURE component/tag -->
<feature>
	<span class="feature">{ i < data.features.length - 1 ? feature + ' | ': feature }</span>

	<!-- JS doesn't have to be in <style> tags (better to have though) -->
	console.log('<feature> created:', this);

	<style scoped>
		span {
			font-weight: bold;
			color: orange;
		}
	</style>
</feature>