<sample>

	<!-- MARKUP -->
	<h1>Riot.js Component(s)</h1>
	<taskItem each={model in tasks}></taskItem>

	<!-- LOGIC -->
	<script>
		console.log('<sample> created:', this);

		/*	Convenience pointer to opts/options
				tasks was passed in to sample.tag
				through the main.js
				riot.mount('tagName', opts); 			*/
				this.tasks = this.opts.tasks;
	</script>

	<!-- STYLES -->
	<style scoped>
		/* CSS rules can be defined, scoped to this tag/component */
	</style>

</sample>